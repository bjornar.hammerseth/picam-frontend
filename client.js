

const searchParams = new URLSearchParams(window.location.search);
room = searchParams.get('room'); // true

const signalingChannel = new WebSocket('wss://go.bjornaahr.com/ws/' + room);

let PeerConnections = {}
let mediaStreams = {}


let stream = null

function addPeerConnection(userID){
  let pc = new RTCPeerConnection({
    iceServers: [
      {
        urls: 'stun:stun.relay.metered.ca:80',
      },
      {
        urls: 'turn:a.relay.metered.ca:443?transport=tcp',
        username: '003cf273ed8fa679b6a7ab9e',
        credential: 'soaLxkL3sYke5HJT',
      },
    ],
  });

  pc.onicecandidate = async(event) => {
    if (event.candidate) {
      let remoteUserID
      console.log(PeerConnections)
      for (let key in PeerConnections) {
        if (PeerConnections[key] === pc) {
          remoteUserID = key
        }
      }

      sendMessageToServer("candidate",event.candidate, remoteUserID)
      console.log('Sent ICE candidate.');

    }
  };

  pc.ontrack = (event) => {
    console.log(event.track.kind)
    let remoteUserID
    for(let key in PeerConnections) {
      if (PeerConnections[key] === pc) {
        remoteUserID = key
      }
    }

    if (event.track.kind === 'video') {
      create_video(remoteUserID, event.track)
    }

    if (event.track.kind === 'audio') {
      create_audio(remoteUserID, event.track)
    }
  };

  pc.oniceconnectionstatechange = async () => {
    console.log('ICE connection state changed:', pc.iceConnectionState);
    if (pc.iceConnectionState === "connected") {
      // Send a message to your server to indicate that ICE negotiation is complete
      // This assumes you have a function `sendMessage` for sending messages to the server
      await signalingChannel.send(JSON.stringify({ type: "connectionEstablished", data: {} }))
      console.log("Sent connection established")
    }
  };

  pc.onconnectionstatechange = () => {
    console.log('Connection state changed:', pc.connectionState);



    if(pc.connectionState === 'failed' || pc.connectionState === 'disconnected'){
    for (let key in PeerConnections) {
      if (PeerConnections[key] === pc) {
        disconnect(key)
        console.log("Removed " + key)
        break; // Remove this line if you want to remove all entries with this value
      }
    }
    }
    document.getElementById('ids').innerHTML = `${JSON.stringify(PeerConnections)}</>\n`

  };

  PeerConnections[userID] = pc
  console.log("Added peer connection")
  return PeerConnections[userID]



}


signalingChannel.onopen = async () => {
  console.log('WebSocket connection opened.');
  stream = await navigator.mediaDevices.getUserMedia({video: true, audio: true});

  create_video(0, stream.getVideoTracks()[0])

  await signalingChannel.send(
      JSON.stringify({ type: 'createUser' , data: {'name': "James"}})
  );
  console.log("Sent createUser")

  await signalingChannel.send(
    JSON.stringify({ type: 'clientReady' , data: {}})
  );
  console.log('Sent clientReady message.');
};

let candidatesQueue = [];

signalingChannel.onmessage = async (event) => {
  const message = JSON.parse(event.data);
  console.log('Received message:', message);

  if (message.type === 'otherClientReady') {
    // The other client is ready. Start the ICE negotiation process.
    console.log("OTHER CLIENT READY")
    console.log(message.data)
    let pc = addPeerConnection(message.data)
    await createAndSendOffer(pc, message.data);
  }

if (message.type === 'answer'){
  const remoteDesc = new RTCSessionDescription(message.data);

  let pc = PeerConnections[message.data["remoteID"]]

  await pc.setRemoteDescription(remoteDesc);
  console.log('Remote description set from answer.');
}



  if (message.type === 'offer') {
    let remoteID = message.data["remoteID"]
    let pc = addPeerConnection(remoteID)
    const remoteDesc = new RTCSessionDescription(message.data);
    await pc.setRemoteDescription(remoteDesc);
    console.log(PeerConnections)
    console.log('Remote description set.');

    try {

      console.log('User media stream obtained.');
    } catch {
      console.log('User media failed')
    }
    for (const track of stream.getTracks()) {
      pc.addTrack(track);
      console.log('Track added' + track.kind);

    }

    // Create an answer and send it to the other client.
    const answer = await pc.createAnswer();
    await pc.setLocalDescription(answer);
    answer["sendTo"] = remoteID
    signalingChannel.send(
        JSON.stringify({type: 'answer', data: {
            sdp: answer.sdp,
            type: answer.type,
            sendTo: remoteID
          }}));
    console.log('Sent answer.');

    // Now process any candidates that might have arrived before the offer
    while (candidatesQueue.length) {
      const candidate = new RTCIceCandidate(candidatesQueue.shift());
      await pc.addIceCandidate(candidate);
      console.log('Added queued ICE candidate.');
    }
  }
  if (message.type === 'candidate') {
    let pc = PeerConnections[message.data["remoteID"]]
    const candidate = new RTCIceCandidate(message.data.candidate);

    if (pc.remoteDescription && pc.remoteDescription.type) {
      // Only add ICE candidates after the remote description has been set
      await pc.addIceCandidate(candidate);
      console.log('Added ICE candidate.');
    } else {
      // If remote description is not set, add the candidate to the queue
      candidatesQueue.push(candidate);
      console.log('Added ICE candidate to queue.');
    }
  }

  if(message.type === 'clientDisconnected'){
    let remoteID = message.data
    disconnect(remoteID)
  }

};





async function createAndSendOffer(pc, remoteID) {
  try {

    for (const track of stream.getTracks()) {
      pc.addTrack(track);
      console.log('Track added' + track.kind);

    }

    const offer = await pc.createOffer();
    await pc.setLocalDescription(offer);
    console.log('Local description set.');
    offer["sendTo"] = remoteID
    console.log(offer)
    signalingChannel.send(
      JSON.stringify({ type: 'offer',     data: {
          sdp: offer.sdp,
          type: offer.type,
          sendTo: remoteID
        }})
    );
    console.log('Sent offer.');
  } catch (error) {
    console.log('Error in creating offer and sending:', error);
  }
}



function sendMessageToServer(messageType, data, sendToId) {
  const messageData = {
    candidate: data,  // The ICE candidate
    sendTo: sendToId  // The ID of the intended recipient
  };
  signalingChannel.send(JSON.stringify({
    type: messageType,
    data: messageData
  }));
}


function disconnect(userID){
  let child = document.getElementById(userID)
  document.getElementById('videos').removeChild(child)
  child = document.getElementById(userID)
  document.getElementById('audios').removeChild(child)
  delete PeerConnections[userID]
  console.log("Removed: " +  userID)
}

function create_audio(userID, track){
  let audioElem = document.createElement("audio");
  audioElem.id = userID;
  audioElem.autoplay = true;
  audioElem.playsInline = true;
  document.getElementById('audios').appendChild(audioElem);

  let remoteAudio = document.getElementById(userID);
  if (!mediaStreams[userID]) {
    // Create a new media stream for this user
    mediaStreams[userID] = new MediaStream();
  }
  // Add the track to the user's media stream
  mediaStreams[userID].addTrack(track);

  remoteAudio.srcObject = mediaStreams[userID];
  console.log('Audio stream received and set.');
}


function  create_video(userID, track){
  let vidDiv = document.createElement("div")
  vidDiv.className = "video-wrapper"
  let vidOverlay = document.createElement("div")
  vidOverlay.className = "video-overlay"
  vidOverlay.textContent = userID
  let videoElem = document.createElement("video");
  videoElem.id = userID;
  videoElem.autoplay = true;
  videoElem.playsInline = true;
  videoElem.poster = "media/video-outline.svg"
  vidDiv.append(videoElem)
  vidDiv.append(vidOverlay)
  document.getElementById('videos').appendChild(vidDiv);
  let remoteVideo = document.getElementById(userID);
  // Check if there's already a media stream for this user
  if (!mediaStreams[userID]) {
    // Create a new media stream for this user
    mediaStreams[userID] = new MediaStream();
  }
  // Add the track to the user's media stream
  mediaStreams[userID].addTrack(track);

  remoteVideo.srcObject = mediaStreams[userID];
  console.log('Video stream received and set.');
}



function mute(){
  stream.getAudioTracks()[0].enabled = !stream.getAudioTracks()[0].enabled
  var img = document.getElementById("muteButton");

  if(stream.getAudioTracks()[0].enabled){
    img.src = "media/microphone.svg"
  } else {
    img.src = "media/microphone-off.svg"
  }
}

function hidecam(){
  stream.getVideoTracks()[0].enabled = !stream.getVideoTracks()[0].enabled
  var img = document.getElementById("hideCameraButton");

  if(stream.getVideoTracks()[0].enabled){
    img.src = "media/video-outline.svg"
  } else {
    img.src = "media/video-off-outline.svg"
  }
}